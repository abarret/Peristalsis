
#ifndef included_ComplexFluidForcing
#define included_ComplexFluidForcing
#include <math.h>
#include <stddef.h>
#include <limits>
#include <ostream>
#include <string>
#include <vector>
#include "AdvDiffOldroydBConvectiveOperator.h"
#include <ibamr/app_namespaces.h>
#include <ibtk/muParserRobinBcCoefs.h>

#include "ArrayData.h"
#include "BoundaryBox.h"
#include "Box.h"
#include "CartesianPatchGeometry.h"
#include "CellData.h"
#include "CellIndex.h"
#include "CellVariable.h"
#include "EdgeData.h"     // IWYU pragma: keep
#include "EdgeGeometry.h" // IWYU pragma: keep
#include "EdgeIndex.h"    // IWYU pragma: keep
#include "HierarchyDataOpsManager.h"
#include "HierarchyDataOpsReal.h"
#include "IBAMR_config.h"
#include "Index.h"
#include "IntVector.h"
#include "LocationIndexRobinBcCoefs.h"
#include "MultiblockDataTranslator.h"
#include "NodeData.h"     // IWYU pragma: keep
#include "NodeGeometry.h" // IWYU pragma: keep
#include "NodeIndex.h"    // IWYU pragma: keep
#include "NodeVariable.h"
#include "Patch.h"
#include "PatchGeometry.h"
#include "PatchHierarchy.h"
#include "PatchLevel.h"
#include "RobinBcCoefStrategy.h"
#include "SideData.h"
#include "Variable.h"
#include "VariableContext.h"
#include "VariableDatabase.h"
#include "ibamr/INSStaggeredHierarchyIntegrator.h"
#include "ibamr/RNG.h"
#include "ibamr/StokesSpecifications.h"
#include "ibamr/ibamr_enums.h"
#include "ibamr/namespaces.h" // IWYU pragma: keep
#include "ibtk/HierarchyGhostCellInterpolation.h"
#include "ibtk/PhysicalBoundaryUtilities.h"
#include "tbox/Array.h"
#include "tbox/Database.h"
#include "tbox/Pointer.h"
#include "tbox/Utilities.h"
#include "ibamr/AdvDiffSemiImplicitHierarchyIntegrator.h"
#if (NDIM == 2)
#include "ibtk/NodeDataSynchronization.h"
#endif
#if (NDIM == 3)
#include "ibtk/EdgeDataSynchronization.h"
#endif
#include <stddef.h>
#include <string>
#include <vector>
#include "ibamr/INSStaggeredHierarchyIntegrator.h"
#include "CellVariable.h"
#include "EdgeVariable.h" // IWYU pragma: keep
#include "IntVector.h"
#include "NodeVariable.h" // IWYU pragma: keep
#include "PatchLevel.h"
#include "VariableContext.h"
#include "ibamr/ibamr_enums.h"
#include "ibtk/CartGridFunction.h"
#include "tbox/Array.h"
#include "tbox/Pointer.h"
#include "ibtk/muParserCartGridFunction.h"
#include "VisItDataWriter.h"
namespace IBAMR
{
class ComplexFluidForcing;
} // namespace IBAMR
namespace SAMRAI
{
namespace hier
{
template <int DIM>
class Patch;
template <int DIM>
class PatchHierarchy;
template <int DIM>
class Variable;
} // namespace hier
namespace tbox
{
class Database;
} // namespace tbox
} // namespace SAMRAI
/////////////////////////////// INCLUDES /////////////////////////////////////

/////////////////////////////// CLASS DEFINITION /////////////////////////////

namespace IBAMR
{
/*!
 * \brief Class INSStaggeredStochasticForcing provides an interface for
 * specifying a stochastic forcing term for a staggered-grid incompressible
 * Navier-Stokes solver.
 */
class ComplexFluidForcing : public IBTK::CartGridFunction
{
public:
    /*!
     * \brief This constructor creates Variable and VariableContext objects for
     * storing the stochastic stresses at the centers and nodes of the Cartesian
     * grid.
     */
    ComplexFluidForcing(const std::string& object_name,
			Pointer< Database > app_initializer,
			const Pointer< IBAMR::INSStaggeredHierarchyIntegrator > fluid_solver,
			Pointer< CartesianGridGeometry<NDIM> > grid_geometry,
			Pointer< IBAMR::AdvDiffSemiImplicitHierarchyIntegrator > adv_diff_integrator,
			Pointer<VisItDataWriter<NDIM> > visit_data_writer);

    SAMRAI::tbox::Pointer<SAMRAI::hier::Variable<NDIM> > getVariable() {
      return d_W_cc_var;
    }
    int getStressTensorDrawVar() {
      return d_W_cc_idx_draw;
    }
    int getVariableIdx() {
      return d_W_cc_idx;
    }
    /*!
     * \brief Empty destructor.
     */
    ~ComplexFluidForcing();

    /*!
     * \name Methods to set patch data.
     */
    //\{

    /*!
     * \brief Indicates whether the concrete INSStaggeredStochasticForcing object is
     * time-dependent.
     */
    bool isTimeDependent() const;

    /*!
     * \brief Evaluate the function on the patch interiors on the specified
     * levels of the patch hierarchy.
     */
    void setDataOnPatchHierarchy(const int data_idx,
                                 SAMRAI::tbox::Pointer<SAMRAI::hier::Variable<NDIM> > var,
                                 SAMRAI::tbox::Pointer<SAMRAI::hier::PatchHierarchy<NDIM> > hierarchy,
                                 const double data_time,
                                 const bool initial_time = false,
                                 const int coarsest_ln = -1,
                                 const int finest_ln = -1);

    /*!
     * \brief Evaluate the function on the patch interior.
     */
    void setDataOnPatch(const int data_idx,
                        SAMRAI::tbox::Pointer<SAMRAI::hier::Variable<NDIM> > var,
                        SAMRAI::tbox::Pointer<SAMRAI::hier::Patch<NDIM> > patch,
                        const double data_time,
                        const bool initial_time = false,
                        SAMRAI::tbox::Pointer<SAMRAI::hier::PatchLevel<NDIM> > patch_level =
                            SAMRAI::tbox::Pointer<SAMRAI::hier::PatchLevel<NDIM> >(NULL));

    //\}

protected:
    /*!
     * The object name is used for error/warning reporting.
     */
    std::string d_object_name;

private:
    /*!
     * \brief Default constructor.
     *
     * \note This constructor is not implemented and should not be used.
     */
    ComplexFluidForcing();

    /*!
     * \brief Copy constructor.
     *
     * \note This constructor is not implemented and should not be used.
     *
     * \param from The value to copy to this object.
     */
    ComplexFluidForcing(const ComplexFluidForcing& from);

    /*!
     * \brief Assignment operator.
     *
     * \note This operator is not implemented and should not be used.
     *
     * \param that The value to assign to this object.
     *
     * \return A reference to this object.
     */
    ComplexFluidForcing& operator=(const ComplexFluidForcing& that);
    /*!
     * Pointer to the fluid solver object that is using this stochastic force
     * generator.
     */
    const INSStaggeredHierarchyIntegrator* const d_fluid_solver;
    /*!
     * VariableContext and Variable objects for storing the components of the
     * stochastic stresses.
     */
    SAMRAI::tbox::Pointer<SAMRAI::hier::VariableContext> d_context;
    int d_W_cc_idx;
    std::vector<int> d_W_cc_idxs;
    IBTK::muParserCartGridFunction* muParser;
    AdvDiffSemiImplicitHierarchyIntegrator* d_adv_diff_integrator;
    SAMRAI::tbox::Pointer<AdvDiffOldroydBConvectiveOperator> d_convec_oper;
    SAMRAI::tbox::Pointer<SAMRAI::pdat::CellVariable<NDIM, double> > d_W_cc_var;
    SAMRAI::tbox::Pointer<SAMRAI::pdat::CellVariable<NDIM, double> > d_W_cc_var_draw;
    int d_W_cc_idx_draw;
    double d_Wi;
    double d_xi;
    int d_W_cc_scratch_idx;
    std::vector<RobinBcCoefStrategy<NDIM>*> d_conc_bc_coefs;
}; //Private

} //Namespace IBAMR
#endif
