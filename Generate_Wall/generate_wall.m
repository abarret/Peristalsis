%%
% parameters

% Length of computational domain
Lx = 1.0;
Ly = 1.0;
% Computational domain info
N = 128;
dx = Lx/N;
dy = Ly/N;
% Lagrangian Info
ds = 0.5*min(dx,dy);
s = 0.0:ds:1.0;
chi = 0.5;
h = 0.25;
alpha = 2*pi*h/Lx;
d = @(xi, t) alpha/(2*pi)*(1+chi*sin(2*pi*(xi-t)));
x_wall = s;
y_wall_up = 0.5+d(s,0.0);
y_wall_low = 0.5-d(s,0.0);
% for t=0:0.01:2
% figure(1); clf;
% y_wall_up = 0.5+d(s,t);
% y_wall_low = 0.5-d(s,t);
% plot(x_wall, y_wall_up);
% hold on;
% plot(x_wall, y_wall_low);
% title(strcat('t=',num2str(t)));
% axis([0 1 0 1]);
% pause(0.01);
% end
kappa_target = 2.0e2;
%%
% write initial structure
fileID = fopen('wall_up.vertex', 'w');
fprintf(fileID, '%d \n', length(x_wall));
for i = 1:length(x_wall)
    fprintf(fileID, '%1.16f %1.16f \n', x_wall(i), y_wall_up(i));
end
fclose(fileID);

fileID = fopen('wall_low.vertex', 'w');
fprintf(fileID, '%d \n', length(x_wall));
for i = 1:length(x_wall)
    fprintf(fileID, '%1.16f %1.16f, \n', x_wall(i), y_wall_low(i));
end
fclose(fileID);
%%
% write target points
fileID = fopen('wall_up.target', 'w');
fprintf(fileID, '%d \n', length(x_wall));
for i = 1:length(x_wall)
    fprintf(fileID, '%d %1.16f \n', i-1, kappa_target*ds/ds^2);
end
fclose(fileID);

fileID = fopen('wall_low.target', 'w');
fprintf(fileID, '%d \n', length(x_wall));
for i = 1:length(x_wall)
    fprintf(fileID, '%d %1.16f \n', i-1, kappa_target*ds/ds^2);
end
fclose(fileID);
%%
% write spring values
fildID = fopen('wall_up.spring', 'w');
fprintf(fileID, '%d \n', length(x_wall)-1);
for i = 1:length(x_wall)-1
    fprintf(fileID, '%d %d %1.16f %1.16f \n', i-1, i, kappa_target*ds/ds^2, sqrt((x_wall(i) - x_wall(i+1)).^2+(y_wall_up(i)-y_wall_up(i+1)).^2));
end
fclose(fileID);

fileID = fopen('wall_low.spring', 'w');
fprintf(fileID, '%d \n', length(x_wall)-1);
for i = 1:length(x_wall)-1
    fprintf(fileID, '%d %d %1.16f %1.16f \n', i-1, i, kappa_target*ds/ds^2, sqrt((x_wall(i) - x_wall(i+1)).^2+(y_wall_low(i)-y_wall_low(i+1)).^2));
end
fclose(fileID);
