#include "update_target_point_positions.h"

#include "ibamr/IBTargetPointForceSpec.h"

#include "ibtk/LData.h"
#define PI 3.141592653589793
void update_target_point_positions(Pointer< PatchHierarchy<NDIM> > hierarchy,
				   LDataManager* lag_manager,
				   const double current_time,
				   const double dt)
{
  const int level_num = hierarchy->getFinestLevelNumber();

  const std::pair<int, int>& wall_low_lag_idxs = lag_manager->getLagrangianStructureIndexRange(0, level_num);
  const std::pair<int, int>& wall_up_lag_idxs = lag_manager->getLagrangianStructureIndexRange(1, level_num);

  // Get mesh
  Pointer<LMesh> l_mesh = lag_manager->getLMesh(level_num);
  const std::vector<LNode*>& local_nodes = l_mesh->getLocalNodes();
  const std::vector<LNode*>& ghost_nodes = l_mesh->getGhostNodes();
  std::vector<LNode*> nodes = local_nodes;
  nodes.insert(nodes.end(), ghost_nodes.begin(), ghost_nodes.end());

  // Loop over mesh nodes and update target positions.
  for(std::vector<LNode*>::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
  {
    const LNode* const node = *it;
    IBTargetPointForceSpec* const force_spec = node->getNodeDataItem<IBTargetPointForceSpec>();
    if(!force_spec) continue;
    double ds = 0.5/128.0;
    double h = 0.25;
    double chi = 0.5;
    double Lx = 1.0;
    double alpha = 2*PI*h/Lx;
    // Update position of target point
    // lag_idx    is index of lagrangian point
    // x_target   is target position of target point
    Point& x_target = force_spec->getTargetPointPosition();
    const int lag_idx = node->getLagrangianIndex();
    if( wall_low_lag_idxs.first <= lag_idx && lag_idx < wall_low_lag_idxs.second)
    {
      // We are on the lower wall
      x_target(0) = lag_idx*ds;
      x_target(1) = 0.5 - alpha/(2*PI)*(1+chi*std::sin(2*PI*(lag_idx*ds-current_time)));

    }
    if (wall_up_lag_idxs.first <= lag_idx && lag_idx < wall_up_lag_idxs.second)
    {
      // We are on the upper wall
      x_target(0) = (lag_idx-wall_up_lag_idxs.first)*ds;
      x_target(1) = 0.5 + alpha/(2*PI)*(1+chi*std::sin(2*PI*((lag_idx-wall_up_lag_idxs.first)*ds-current_time)));
    }
  }
}
