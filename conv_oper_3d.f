c234567
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c	Computes cell centered Oldroyd-B Convective Operator
c
c	where u is vector valued face centered velocity
c       and tau is symmetric tensor valued cell centered
c       
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine conv_oper_3d(dx, u_data_0, u_data_1, u_data_2,
     &        u_gcw, s_data, s_gcw, r_data, r_gcw, ilower0, 
     &        iupper0, ilower1, iupper1, ilower2, iupper2, wi)
     
      parameter (NDIM = 3)
      Integer ilower0, iupper0
      Integer ilower1, iupper1
      Integer ilower2, iupper2

      real*8 wi
      real*8 dx(0:NDIM-1)

c     
c    Velocity Data
c
      Integer u_gcw
      real*8 u_data_0((ilower0-u_gcw):(iupper0+u_gcw+1),
     &                (ilower1-u_gcw):(iupper1+u_gcw),
     &                (ilower2-u_gcw):(iupper2+u_gcw))
      real*8 u_data_1((ilower1-u_gcw):(iupper1+u_gcw+1),
     &                (ilower2-u_gcw):(iupper2+u_gcw),
     &                (ilower0-u_gcw):(iupper0+u_gcw))
      real*8 u_data_2((ilower2-u_gcw):(iupper2+u_gcw+1),
     &                (ilower0-u_gcw):(iupper0+u_gcw),
     &                (ilower1-u_gcw):(iupper1+u_gcw))
c     
c    Tensor Data
c
      Integer s_gcw
      real*8 s_data((ilower0-s_gcw):(iupper0+s_gcw),
     &		   (ilower1-s_gcw):(iupper1+s_gcw),
     &             (ilower2-s_gcw):(iupper2+s_gcw),
     &              0:5)
c
c    Return Data
c
      Integer r_gcw
      real*8 r_data((ilower0-r_gcw):(iupper0+r_gcw),
     &		   (ilower1-r_gcw):(iupper1+r_gcw),
     &             (ilower2-r_gcw):(iupper2+r_gcw),
     &              0:5)
    
     
      Integer i0, i1, i2
      real*8 u_ij, v_ij, w_ij
      real*8 dq_xx_dx, dq_yy_dx, dq_zz_dx
      real*8 dq_yz_dx, dq_xz_dx, dq_xy_dx
      real*8 dq_xx_dy, dq_yy_dy, dq_zz_dy
      real*8 dq_yz_dy, dq_xz_dy, dq_xy_dy
      real*8 dq_xx_dz, dq_yy_dz, dq_zz_dz
      real*8 dq_yz_dz, dq_xz_dz, dq_xy_dz
      real*8 du_dx, dv_dx, dw_dx
      real*8 du_dy, dv_dy, dw_dy
      real*8 du_dz, dv_dz, dw_dz
      real*8 scalex_q, scaley_q, scalez_q
      real*8 scale_ux, scale_uy, scale_uz
      real*8 scale_vx, scale_vy, scale_yz
      real*8 scale_wx, scale_wy, scale_wz
      real*8 wi_inv
      real*8 qxx_ij, qyy_ij, qzz_ij
      real*8 qyz_ij, qxz_ij, qxy_ij
      
      wi_inv = 1.d0/wi
      scale_ux = 1.d0/dx(0)
      scale_uy = 1.d0/(4.d0*dx(1))
      scale_uz = 1.d0/(4.d0*dx(2))
      scale_vx = 1.d0/(4.d0*dx(0))
      scale_vy = 1.d0/dx(1)
      scale_vz = 1.d0/(4.d0*dx(2))
      scale_wx = 1.d0/(4.d0*dx(0))
      scale_wy = 1.d0/(4.d0*dx(1))
      scale_wz = 1.d0/dx(2)
      scalex_q = 1.d0/(2.d0*dx(0))
      scaley_q = 1.d0/(2.d0*dx(1))
      scalez_q = 1.d0/(2.d0*dx(2))
      do i2 = ilower2, iupper2
	do i1 = ilower1, iupper1
	  do i0 = ilower0, iupper0
            u_ij = (u_data_0(i0,i1,i2)+u_data_0(i0+1,i1,i2))*0.5
	    du_dx = 
     &        scale_ux*(u_data_0(i0+1,i1,i2)-u_data_0(i0,i1,i2))
            du_dy = 
     &        scale_uy*(u_data_0(i0+1,i1+1,i2)+u_data_0(i0,i1+1,i2)
     &              -u_data_0(i0+1,i1-1,i2)-u_data_0(i0,i1-1,i2))
	    du_dz = 
     &        scale_uz*(u_data_0(i0+1,i1,i2+1)+u_data_0(i0,i1,i2+1)
     &	            -u_data_0(i0+1,i1,i2-1)-u_data_0(i0,i1,i2-1))
	    v_ij = (u_data_1(i1,i2,i0)+u_data_1(i1+1,i2,i0))*0.5
            dv_dy = 
     &        scale_vy*(u_data_1(i1+1,i2,i0)-u_data_1(i1,i2,i0))
            dv_dx = 
     &        scale_vx*(u_data_1(i1+1,i2,i0+1)+u_data_1(i1,i2,i0+1)
     &              -u_data_1(i1+1,i2,i0-1) - u_data_1(i1,i2,i0-1))
            dv_dz = 
     &        scale_vz*(u_data_1(i1+1,i2+1,i0)+u_data_1(i1,i2+1,i0)
     &              -u_data_1(i1+1,i2-1,i0) - u_data_1(i1,i2-1,i0))
	    w_ij = (u_data_2(i2,i0,i1)+u_data_2(i2+1,i0,i1))*0.5
            dw_dx = 
     &        scale_wx*(u_data_2(i2+1,i0+1,i1)+u_data_2(i2,i0+1,i1)
     &              -u_data_2(i2+1,i0-1,i1)-u_data_2(i2,i0-1,i1))
            dw_dy = 
     &        scale_wy*(u_data_2(i2+1,i0,i1+1)+u_data_2(i2,i0,i1+1)
     &              -u_data_2(i2+1,i0,i1-1)-u_data_2(i2,i0,i1-1))
            dw_dz = 
     &        scale_wz*(u_data_2(i2+1,i0,i1)-u_data_2(i2,i0,i1))
            dq_xx_dx = scalex_q*(s_data(i0+1,i1,i2,0)
     &                 -s_data(i0-1,i1,i2,0))
            dq_xx_dy = scaley_q*(s_data(i0,i1+1,i2,0)
     &                 -s_data(i0,i1-1,i2,0))
            dq_xx_dz = scalez_q*(s_data(i0,i1,i2+1,0)
     &                 -s_data(i0,i1,i2-1,0))
	    qxx_ij = s_data(i0,i1,i2,0)
            dq_yy_dx = scalex_q*(s_data(i0+1,i1,i2,1)
     &                 -s_data(i0-1,i1,i2,1))
            dq_yy_dy = scaley_q*(s_data(i0,i1+1,i2,1)
     &                 -s_data(i0,i1-1,i2,1))
            dq_yy_dz = scalez_q*(s_data(i0,i1,i2+1,1)
     &                 -s_data(i0,i1,i2-1,1))
	    qyy_ij = s_data(i0,i1,i2,1)
            dq_zz_dx = scalex_q*(s_data(i0+1,i1,i2,2)
     &                 -s_data(i0-1,i1,i2,2))
            dq_zz_dy = scaley_q*(s_data(i0,i1+1,i2,2)
     &                 -s_data(i0,i1-1,i2,2))
            dq_zz_dz = scalez_q*(s_data(i0,i1,i2+1,2)
     &                 -s_data(i0,i1,i2-1,2))
	    qzz_ij = s_data(i0,i1,i2,2)
            dq_yz_dx = scalex_q*(s_data(i0+1,i1,i2,3)
     &                 -s_data(i0-1,i1,i2,3))
            dq_yz_dy = scaley_q*(s_data(i0,i1+1,i2,3)
     &                 -s_data(i0,i1-1,i2,3))
            dq_yz_dz = scalez_q*(s_data(i0,i1,i2+1,3)
     &                 -s_data(i0,i1,i2-1,3))
	    qyz_ij = s_data(i0,i1,i2,3)
            dq_xz_dx = scalex_q*(s_data(i0+1,i1,i2,4)
     &                 -s_data(i0-1,i1,i2,4))
            dq_xz_dy = scaley_q*(s_data(i0,i1+1,i2,4)
     &                 -s_data(i0,i1-1,i2,4))
            dq_xz_dz = scalez_q*(s_data(i0,i1,i2+1,4)
     &                 -s_data(i0,i1,i2-1,4))
	    qxz_ij = s_data(i0,i1,i2,4)
            dq_xy_dx = scalex_q*(s_data(i0+1,i1,i2,5)
     &                 -s_data(i0-1,i1,i2,5))
            dq_xy_dy = scaley_q*(s_data(i0,i1+1,i2,5)
     &                 -s_data(i0,i1-1,i2,5))
            dq_xy_dz = scalez_q*(s_data(i0,i1,i2+1,5)
     &                 -s_data(i0,i1,i2-1,5))
	    qxy_ij = s_data(i0,i1,i2,5)

            r_data(i0,i1,i2,0) = 
     &        u_ij*dq_xx_dx + v_ij*dq_xx_dy + w_ij*dq_xx_dz
     &        - 2.d0*du_dx*qxx_ij - 2.d0*du_dy*qxy_ij
     &        - 2.d0*du_dz*qxz_ij - wi_inv
	    r_data(i0,i1,i2,1) = 
     &        u_ij*dq_yy_dx + v_ij*dq_yy_dy + w_ij*dq_yy_dz 
     &        - 2.d0*dv_dx*qxy_ij - 2.d0*dv_dy*qyy_ij
     &        - 2.d0*dv_dz*qyz_ij - wi_inv
            r_data(i0,i1,i2,2) = 
     &        u_ij*dq_zz_dx + v_ij*dq_zz_dy + w_ij*dq_zz_dz
     &        - 2.d0*dw_dx*qxz_ij - 2.d0*dw_dy*qyz_ij
     &        - 2.d0*dw_dz*qzz_ij - wi_inv
            r_data(i0,i1,i2,3) = 
     &        u_ij*dq_yz_dx + v_ij*dq_yz_dy + w_ij*dq_yz_dz
     &        - qyy_ij*dw_dy - qzz_ij*dv_dz
     &        + qyz_ij*du_dx - qxz_ij*dv_dx
     &        - qxy_ij*dw_dx
            r_data(i0,i1,i2,4) = 
     &        u_ij*dq_xz_dx + v_ij*dq_xz_dy + w_ij*dq_xz_dz
     &        - qxx_ij*dw_dx - qzz_ij*du_dz
     &        + qxz_ij*dv_dy - qyz_ij*du_dy
     &        - qxy_ij*dw_dy
            r_data(i0,i1,i2,5) = 
     &        u_ij*dq_xy_dx + v_ij*dq_xy_dy + w_ij*dq_xy_dz
     &        - qxx_ij*dv_dx - qyy_ij*du_dy
     &        + qxy_ij*dw_dz - qxz_ij*dv_dz
     &        - qyz_ij*du_dz
	  enddo
        enddo
      enddo
      end subroutine