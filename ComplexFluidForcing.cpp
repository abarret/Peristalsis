#include "ComplexFluidForcing.h"

extern "C" {
#if(NDIM == 2)
  void div_tensor_2d_(const double* dx,
	      const double* d_data_0,
	      const double* d_data_1,
	      const int& d_gcw,
	      const double* s_data,
	      const int& s_gcw,
	      const int& ilower0,
	      const int& iupper0,
	      const int& ilower1,
	      const int& iupper1,
	      const double& alpha);
#endif
#if(NDIM == 3)
  void div_tensor_3d_(const double* dx,
		      const double* d_data_0,
		      const double* d_data_1,
		      const double* d_data_2,
		      const int& d_gcw,
		      const double* s_data,
		      const int& s_gcw,
		      const int& ilower0,
		      const int& iupper0,
		      const int& ilower1,
		      const int& iupper1,
		      const int& ilower2,
		      const int& iupper2,
		      const double& alpha);
#endif
}
//Namespace
namespace IBAMR
{
//Constructor
ComplexFluidForcing::ComplexFluidForcing(const std::string& object_name, Pointer<Database> input_db,
					 const Pointer< INSStaggeredHierarchyIntegrator> fluid_solver,
					 Pointer<CartesianGridGeometry<NDIM> > grid_geometry,
					 Pointer<AdvDiffSemiImplicitHierarchyIntegrator> adv_diff_integrator,
					 Pointer<VisItDataWriter<NDIM> > visit_data_writer
					)
: d_object_name(object_name), d_adv_diff_integrator(adv_diff_integrator), d_context(NULL), d_W_cc_var(NULL),
  d_W_cc_idx(-1), d_Wi(-1), d_fluid_solver(fluid_solver), d_xi(-1)
{
  // Set up muParserCartGridFunctions
  muParser = new muParserCartGridFunction(object_name, input_db->getDatabase("InitialConditions"), grid_geometry);
  d_Wi = input_db->getDouble("Wi");
  d_xi = input_db->getDouble("Xi");
  // Register Variables and variable context objects.
  VariableDatabase<NDIM>* var_db = VariableDatabase<NDIM>::getDatabase();
  d_context = var_db->getContext(d_object_name + "::CONTEXT");
  d_W_cc_var = new CellVariable<NDIM, double>(d_object_name + "::W_cc", NDIM*(NDIM+1)/2);
  static const IntVector<NDIM> ghosts_cc = 1;
  d_W_cc_idx = var_db->registerVariableAndContext(d_W_cc_var, d_context, ghosts_cc);
  // Set up Advection Diffusion Integrator
  const ConvectiveDifferencingType difference_type = IBAMR::string_to_enum<ConvectiveDifferencingType>(input_db->getStringWithDefault("difference_form", IBAMR::enum_to_string<ConvectiveDifferencingType>(ADVECTIVE)));
  d_adv_diff_integrator->registerTransportedQuantity(d_W_cc_var);
  d_adv_diff_integrator->setDiffusionCoefficient(d_W_cc_var, input_db->getDoubleWithDefault("D", 0.0));
  d_adv_diff_integrator->setInitialConditions(d_W_cc_var, muParser);
  d_adv_diff_integrator->setDampingCoefficient(d_W_cc_var, 1.0/d_Wi);
  d_adv_diff_integrator->setAdvectionVelocity(d_W_cc_var, d_fluid_solver->getAdvectionVelocityVariable());
  d_W_cc_var_draw = new CellVariable<NDIM, double>(d_object_name + "::W_cc_draw", NDIM*NDIM);
  d_W_cc_idx_draw = var_db->registerVariableAndContext(d_W_cc_var_draw, d_context, IntVector<NDIM>(0));
  visit_data_writer->registerPlotQuantity("Stress Tensor", "TENSOR", d_W_cc_idx_draw);
  LocationIndexRobinBcCoefs<NDIM> bc_coef(d_object_name + "::bc_coef", Pointer<Database> (NULL));
  for (int d = 0; d < NDIM; ++d)
  {
    bc_coef.setBoundarySlope(2*d, 0.0);
    bc_coef.setBoundarySlope(2*d+1,0.0);
  }
  std::vector<RobinBcCoefStrategy<NDIM>*> bc_coefs(NDIM*(NDIM+1)/2, &bc_coef);
  // Create boundary conditions for advected materials if not periodic
  const IntVector<NDIM>& periodic_shift = grid_geometry->getPeriodicShift();
  if(periodic_shift.min() <= 0)
  {
    vector<RobinBcCoefStrategy<NDIM>*> conc_bc_coefs(NDIM*(NDIM+1)/2);
    for (int d = 0; d < NDIM*(NDIM+1)/2; ++d)
    {
      ostringstream conc_bc_name;
      conc_bc_name << "c_bc_coef_" << d;
      const string bc_coefs_name = conc_bc_name.str();

      ostringstream bc_coefs_db_name_stream;
      bc_coefs_db_name_stream << "ExtraStressBoundaryConditions_" << d;
      const string bc_coefs_db_name = bc_coefs_db_name_stream.str();

      conc_bc_coefs[d] = new muParserRobinBcCoefs(bc_coefs_name, input_db->getDatabase(bc_coefs_db_name), grid_geometry);
    }
    d_adv_diff_integrator->setPhysicalBcCoefs(d_W_cc_var, conc_bc_coefs);
  }
  Pointer<AdvDiffOldroydBConvectiveOperator> convec_oper = new AdvDiffOldroydBConvectiveOperator("OldroydBConvectiveOperator",d_W_cc_var, input_db, difference_type, d_adv_diff_integrator->getPhysicalBcCoefs(d_W_cc_var), d_Wi, d_fluid_solver->getAdvectionVelocityVariable()); 
  d_adv_diff_integrator->setConvectiveOperator(d_W_cc_var, convec_oper);
  return;
} //End Constructor
// Destructor
ComplexFluidForcing::~ComplexFluidForcing() {
  // intentionally blank
  return;
} // End Destructor

// Time Dependent?
bool ComplexFluidForcing::isTimeDependent() const {
  return true;
} // End Time Dependent?

// setDataOnPatchHierarchy
void ComplexFluidForcing::setDataOnPatchHierarchy(const int data_idx, Pointer<Variable<NDIM> > var,
						  Pointer<PatchHierarchy<NDIM> > hierarchy, const double data_time,
						  const bool initial_time, const int coarsest_ln_in, const int finest_ln_in)
{
  const int coarsest_ln = (coarsest_ln_in == -1 ? 0 : coarsest_ln_in);
  const int finest_ln = (finest_ln_in == -1 ? hierarchy->getFinestLevelNumber() : finest_ln_in);
  VariableDatabase<NDIM>* var_db = VariableDatabase<NDIM>::getDatabase();
  static const IntVector<NDIM> ghosts_cc = 1;
  d_W_cc_idx = var_db->mapVariableAndContextToIndex(d_W_cc_var, d_adv_diff_integrator->getCurrentContext());
  d_W_cc_scratch_idx = var_db->registerVariableAndContext(d_W_cc_var, d_context, ghosts_cc);

  // Allocate Data to store components of the Complex stress tensor
  for (int level_num = coarsest_ln; level_num <= finest_ln; ++level_num)
  {
    Pointer<PatchLevel<NDIM> > level = hierarchy->getPatchLevel(level_num);
    if (!level->checkAllocated(d_W_cc_scratch_idx)) level->allocatePatchData(d_W_cc_scratch_idx);
    if (!level->checkAllocated(d_W_cc_idx_draw)) level->allocatePatchData(d_W_cc_idx_draw);
    if (!level->checkAllocated(d_W_cc_idx)) level->allocatePatchData(d_W_cc_idx);
  }
  if(initial_time)
    muParser->setDataOnPatchHierarchy(d_W_cc_idx, d_W_cc_var, hierarchy, data_time);

  HierarchyDataOpsManager<NDIM>* hier_data_ops_manager = HierarchyDataOpsManager<NDIM>::getManager();
  Pointer<HierarchyDataOpsReal<NDIM, double> > hier_cc_data_ops =
  hier_data_ops_manager->getOperationsDouble(d_W_cc_var, hierarchy, true);
  hier_cc_data_ops->copyData(d_W_cc_scratch_idx, d_W_cc_idx);

  typedef HierarchyGhostCellInterpolation::InterpolationTransactionComponent InterpolationTransactionComponent;
  std::vector<InterpolationTransactionComponent> ghost_cell_components(1);
  ghost_cell_components[0] = InterpolationTransactionComponent(d_W_cc_scratch_idx, "CONSERVATIVE_LINEAR_REFINE", false, "CONSERVATIVE_COARSEN", "LINEAR", false, d_conc_bc_coefs, NULL);
  HierarchyGhostCellInterpolation ghost_fill_op;
  ghost_fill_op.initializeOperatorState(ghost_cell_components, hierarchy);
  ghost_fill_op.fillData(data_time);

  // Compute Div W on each patch level
  for (int level_num = coarsest_ln; level_num <= finest_ln; ++level_num)
  {
    setDataOnPatchLevel(data_idx, var, hierarchy->getPatchLevel(level_num), data_time, initial_time);
  }
  return;
} // End setDataOnPatchHierarchy

void ComplexFluidForcing::setDataOnPatch(const int data_idx, Pointer<Variable<NDIM> > /*var*/, Pointer<Patch<NDIM> > patch,
					 const double data_time, const bool initial_time, Pointer<PatchLevel<NDIM> > /*patch_level*/)
{
  const Box<NDIM>& patch_box = patch->getBox();
  const Pointer<CartesianPatchGeometry<NDIM> > p_geom = patch->getPatchGeometry();
  const double* dx = p_geom->getDx();
  Pointer<SideData<NDIM, double> > divW_sc_data = patch->getPatchData(data_idx);
  divW_sc_data->fillAll(0.0);
  if (initial_time)
    return;
  Pointer<CellData<NDIM, double> > W_cc_data = patch->getPatchData(d_W_cc_scratch_idx);
  Pointer<CellData<NDIM, double> > W_cc_data_draw = patch->getPatchData(d_W_cc_idx_draw);
#if (NDIM == 2)
  W_cc_data_draw->copyDepth(0,*W_cc_data,0);
  W_cc_data_draw->copyDepth(1,*W_cc_data,2);
  W_cc_data_draw->copyDepth(2,*W_cc_data,2);
  W_cc_data_draw->copyDepth(3,*W_cc_data,1);
#endif
#if (NDIM == 3)
  W_cc_data_draw->copyDepth(0,*W_cc_data,0);
  W_cc_data_draw->copyDepth(1,*W_cc_data,5);
  W_cc_data_draw->copyDepth(2,*W_cc_data,4);
  W_cc_data_draw->copyDepth(3,*W_cc_data,5);
  W_cc_data_draw->copyDepth(4,*W_cc_data,1);
  W_cc_data_draw->copyDepth(5,*W_cc_data,3);
  W_cc_data_draw->copyDepth(6,*W_cc_data,4);
  W_cc_data_draw->copyDepth(7,*W_cc_data,3);
  W_cc_data_draw->copyDepth(8,*W_cc_data,2);
#endif
  const IntVector<NDIM> W_cc_ghosts = W_cc_data->getGhostCellWidth();
  const IntVector<NDIM> divW_sc_ghosts = divW_sc_data->getGhostCellWidth();
  double alpha = d_xi/d_Wi;
  const IntVector<NDIM>& patch_lower = patch_box.lower();
  const IntVector<NDIM>& patch_upper = patch_box.upper();
#if (NDIM == 2)
      div_tensor_2d_(dx, divW_sc_data->getPointer(0), divW_sc_data->getPointer(1), divW_sc_ghosts.min(),
		     W_cc_data->getPointer(0), W_cc_ghosts.min(), patch_lower(0), patch_upper(0),
		     patch_lower(1), patch_upper(1), alpha);
#endif
#if (NDIM == 3)
      div_tensor_3d_(dx, divW_sc_data->getPointer(0), divW_sc_data->getPointer(1), divW_sc_data->getPointer(2),
		     divW_sc_ghosts.min(), W_cc_data->getPointer(0), W_cc_ghosts.min(), patch_lower(0),
		     patch_upper(0), patch_lower(1), patch_upper(1), patch_lower(2), patch_upper(2), alpha);
#endif
}// end setDataOnPatch

}//End Namespace
